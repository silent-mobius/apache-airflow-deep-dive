# setup scenario

---
# what is airflow


---
# why use airflow

- dynamic
- scalable
- ui
- extensibility

---

# Internals components

- websserver
- scheduler
- metastore
- executor
- worker

---
# DAG Dynamic Aclycic Graph

---

#what airflow is not ?
- not a data streaming solution
- not a data processing framework
  
---

#how it works

- node:
  - webserver
  - metastore
  - scheduler
  - executor
    - queue